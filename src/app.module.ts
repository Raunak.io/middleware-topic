import { Module,NestModule,MiddlewareConsumer } from '@nestjs/common';

import {TestMiddleware,functMiddleware} from './test.middleware';


import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {

configure(consumer:MiddlewareConsumer){
consumer.apply(TestMiddleware,functMiddleware).forRoutes('test') // i have set it to default route
// consumer.apply(TestMiddleware,functMiddleware).forRoutes({path:'',method:RequestMethod.GET});
// consumer.apply(TestMiddleware,functMiddleware).forRoutes({path:'ab*cd',method:RequestMethod.ALL});
//the last one is used for wildcard entries for basically pattern based routes
// consumer.apply(TestMiddleware,functMiddleware).forRoutes(AppController); can chain multiple controllers |routes|paths n so on
// note: in apply() we can chain multiple middlewares as well
// consumer.apply(cors(),helmet(),TestMiddleware).exclude({path:':id',method:RequestMethod.Post},{path:':id',method:RequestMethod.Patch});

// check main.ts for more
}

}
