import {Injectable ,NestMiddleware} from '@nestjs/common';
import {Request,Response} from 'express';


@Injectable()

export class TestMiddleware implements NestMiddleware {

use(req:Request,res:Response,next:Function){

console.log('you are passing through a middleware function',console.log(req));

next();

}



}

////   we can use functional middlewares if we dont have any additional methods|dependencies attached to it

export function functMiddleware(req:Request,res:Response,next:Function){

console.log('you are passing through a functional middleware');

next();


}