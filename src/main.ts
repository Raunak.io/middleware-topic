import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {functMiddleware} from './test.middleware';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

app.use(functMiddleware); // it is applicable for every route in this application


  await app.listen(3000);
}
bootstrap();
